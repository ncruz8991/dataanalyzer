#pragma once

#include <string>

extern const std::string CONFIG_PATH;
extern const std::string DATA_CONFIG_PATH;

// This is a set of functions that uses the windows .ini reader to read and write specific
// key/value pairs to a configuration file

bool LoadConfigString(std::string &value, const std::string &section, const std::string &key, const std::string &file);
bool LoadConfigInteger(int &value, const std::string &section, const std::string &key, const std::string &file);
bool LoadConfigFloat(float &value, const std::string &section, const std::string &key, const std::string &file);
bool LoadConfigBoolean(bool &value, const std::string &section, const std::string &key, const std::string &file);

void WriteConfigString(const std::string &value, const std::string &section, const std::string &key, const std::string &file);
void WriteConfigInteger(int value, const std::string &section, const std::string &key, const std::string &file);
void WriteConfigFloat(float value, const std::string &section, const std::string &key, const std::string &file);
void WriteConfigBoolean(bool value, const std::string &section, const std::string &key, const std::string &file);