#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "DataAnalyzer.h"

using namespace std;

static const int numCols = 26;
static int masterNumHeaders;

string csv_files[16] =
{
	"log_Scenario_A.csv", // 0
	"log_Scenario_B.csv", // 1
	"log_Scenario_C.csv", // 2
	"log_Scenario_D.csv", // 3

	"log_Scenario_E.csv", // 4
	"log_Scenario_F.csv", // 5
	"log_Scenario_G.csv", // 6
	"log_Scenario_H.csv", // 7

	"log_Scenario_I.csv", // 8
	"log_Scenario_J.csv", // 9
	"log_Scenario_K.csv", // 10
	"log_Scenario_L.csv", // 11

	"log_Scenario_M.csv", // 12
	"log_Scenario_N.csv", // 13
	"log_Scenario_O.csv", // 14
	"log_Scenario_P.csv"  // 15
};

// adds data from CVS's
// This can be used to access any cell in any file.
// vector[file][row][column]
void readDataIntoVector(vector<DataAnalyzer>& v)
{
	//for (unsigned i = 8; i < 9; ++i)
	for (unsigned i = 0; i < CSV_FILE_COUNT; ++i)
	{
		v.emplace_back(csv_files[i], numCols, 1.0);
	}
}

void replaceSpaces(string& s)
{
	for(int i = 0; i < s.length(); i++)
        if(s[i] == ' ')
			s[i] = '_';
}

// Do all of your averaging and stuff here
void initMasterLog(ofstream& log)
{
	string master_log_name("master_log_");
	string participant;
	LoadConfigString(participant, "General", "participant", CONFIG_PATH);
	replaceSpaces(participant);
	master_log_name += participant;
	master_log_name += ".csv";
	log = ofstream(master_log_name.c_str(), ofstream::out);

	vector<string> headers;
	headers.emplace_back("logID");
	headers.emplace_back("Alarm Type");
	headers.emplace_back("Participant Name");
	headers.emplace_back("Scenario Name");

	headers.emplace_back("Autopilot");
	headers.emplace_back("Surprise");
	headers.emplace_back("Aural");
	headers.emplace_back("Graded");

	headers.emplace_back("Runway");
	headers.emplace_back("Blunder Direction");

	headers.emplace_back("Accuracy");

	/***** Signal Stats *****/

	headers.emplace_back("Time at which the first graded cue was sent (seconds)");
	headers.emplace_back("Altitude at which the first graded cue was sent (feet)");
	headers.emplace_back("Indicated Airspeed at which the first graded cue was sent (knots)");
	headers.emplace_back("LogID at which the first graded cue was sent");

	headers.emplace_back("Time at which the tactile cue was sent (seconds)");
	headers.emplace_back("Altitude at which the tactile cue was sent (feet)");
	headers.emplace_back("Indicated Airspeed when tactile cue was sent (knots)");
	headers.emplace_back("LogID when tactile cue was sent");

	/***** Flight Quality *****/

	headers.emplace_back("Average Deviation from Localizer (GS capture to 600/alert)");
	headers.emplace_back("Average Deviation from Glide Slope (GS capture to 600/alert)");
	headers.emplace_back("Average of Average Deviation from LOC and GS (GS capture to 600/alert)");

	headers.emplace_back("Standard Deviation from Localizer (GS capture to 600/alert)");
	headers.emplace_back("Standard Deviation from Glide Slope (GS capture to 600/alert)");
	headers.emplace_back("Standard Deviation of Average from LOC and GS (GS capture to 600/alert)");

	/***** Response Times *****/

	headers.emplace_back("RT_pitch_raw");
	headers.emplace_back("RT_roll_raw");
	headers.emplace_back("RT_glideslope_raw");
	headers.emplace_back("RT_glideslope_velocity");
	headers.emplace_back("RT_localizer_raw");
	headers.emplace_back("RT_localizer_velocity");

	headers.emplace_back("RT_horizontal");
	headers.emplace_back("RT_vertical");
	headers.emplace_back("RT_master");

	/***** Go-Around Quality *****/	

	//headers.emplace_back("Standard Deviation of Vertical Speed at go around");
	//headers.emplace_back("Did airspeed go below 100 knots?");

	// Add them to the log
	for (unsigned i = 0; i < headers.size(); ++i)
	{
		if (i == headers.size() - 1)
			log << headers[i].c_str();
		else
			log << headers[i].c_str() << ',';
	}
	log << '\n';
}

static int masterLogID = 1;
void writeToMasterLog(const ResultsPackage& rp, ofstream& log)
{	
	log << masterLogID++ << ',';
	log << rp.alarm_type << ',';
	log << rp.participant << ',';
	log << rp.scenario << ',';

	log << rp.autopilot << ',';
	log << rp.surprise << ',';
	log << rp.aural << ',';
	log << rp.graded << ',';

	log << rp.runway << ',';
	log << rp.blunder << ',';

	log << rp.accurate<< ',';

	/***** Signal Stats *****/

	log << rp.low_time << ',';
	log << rp.low_altitude << ',';
	log << rp.low_airspeed << ',';
	log << rp.low_logID << ',';

	log << rp.high_time << ',';
	log << rp.high_altitude << ',';
	log << rp.high_airspeed << ',';
	log << rp.high_logID << ',';

	/***** Flight Quality *****/

	log << rp.average_deviation_localizer << ',';
	log << rp.average_deviation_glideslope << ',';
	log << rp.average_deviation_both << ',';

	log << rp.standard_deviation_localizer << ',';
	log << rp.standard_deviation_glideslope << ',';
	log << rp.standard_deviation_both << ',';

	/***** Response Times *****/

	// Yoke Pitch Input
	log << rp.RT_pitch_raw << ',';

	// Yoke Roll Input
	log << rp.RT_roll_raw << ',';

	// Glideslope
	log << rp.RT_glideslope_raw << ',';
	log << rp.RT_glideslope_vel << ',';

	// Localizer
	log << rp.RT_localizer_raw << ',';
	log << rp.RT_localizer_vel << ',';

	log << rp.RT_horizontal << ',';
	log << rp.RT_vertical << ',';
	log << rp.RT_master << ',';

	/***** Go-Around Quality *****/

	//log << rp.standard_deviation_vertical_speed << ',';
	//log << rp.stall << ',';
	log << '\n';
}

int main() {
	
	// Create a single file to put all of the data calculations in
	ofstream master_log;
	initMasterLog(master_log);

	// Make a DataAnalyzer for every file in the directory
	vector<DataAnalyzer> all_CSVs;
	vector<int> only_manual;
	//For the manual runs, this vector stores the standard dev 30 sec pre-tactor
	vector<float> manualDeviationsPitch;
	vector<float> manualDeviationsRoll;
	readDataIntoVector(all_CSVs);

	// Do stuff with them
	int total = all_CSVs.size();
	for (int i = 0; i < total; ++i)
	{
		DataAnalyzer& cur = all_CSVs[i];
		cur.run();
		ResultsPackage results = cur.finalizeResults();

		if(cur.isManualRun() && cur.HasBlunder())
			only_manual.push_back(i);

		writeToMasterLog(results, master_log);
	}

	//After master log has been written, re-perform response time analysis on manual runs
	int manuals = only_manual.size();
	for(int i = 0; i < manuals; i++)
		manualDeviationsPitch.push_back(all_CSVs[only_manual[i]].getPitchDev());
	for(int i = 0; i < manuals; i++)
		manualDeviationsRoll.push_back(all_CSVs[only_manual[i]].getRollDev());


	float averagePitch = 0.0;
	float averageRoll = 0.0;
	//Calculate the average standard deviation over all manual runs for pitch and roll
	
	//There's almost certainly a better way to do this
	averagePitch = getAverage(manualDeviationsPitch);
	averageRoll = getAverage(manualDeviationsRoll);

	//Recalculate response time using the average standard deviation, and append results to master log
	for(int i = 0; i < total; i++){
		DataAnalyzer& cur = all_CSVs[i];
		cur.setAvgStdDevPitch(averagePitch);
		cur.setAvgStdDevRoll(averageRoll);

		//Same as run, but uses the average std devs for pitch & roll instead
		//cur.run();
		cur.runAvg();
		ResultsPackage results = cur.finalizeResults();
		writeToMasterLog(results, master_log);
	}

	//Recalculate response time using the GS and Localizer datas
	for(int i = 0; i < total; i++){
		DataAnalyzer& cur = all_CSVs[i];
		cur.setAvgStdDevPitch(averagePitch);
		cur.setAvgStdDevRoll(averageRoll);

		//Same as run, but uses the average std devs for pitch & roll instead
		//cur.run();
		cur.runAvg();
		ResultsPackage results = cur.finalizeResults();
		writeToMasterLog(results, master_log);
	}	
	/*
	DataAnalyzer scenario_c(csv_files[CSV_C], numCols);

	scenario_c.run();
	cout << endl << "Finished analyzing " << csv_files[CSV_C] << endl;
	ResultsPackage results = scenario_c.finalizeResults();
	
	writeToMasterLog(results, master_log);
	*/
	master_log.close();

	//int x;
	//std::cin >> x;
	return 0;
}

double getAverage(vector<float> & data){
	for(float & dev : manualDeviationsPitch)
		averagePitch += dev;
	averagePitch /= data.size();
	return averagePitch;
}