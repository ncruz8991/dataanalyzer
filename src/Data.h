#pragma once
#include <string>
using namespace std;

// This is a package that DataAnalyzer can send to main
// after it's finished running calculations.
// The variables in this are used for the master log.
struct ResultsPackage 
{
	string alarm_type;
	string participant;
	string scenario;

	string autopilot;
	string surprise;
	string aural;
	string graded;

	string runway;
	string blunder;

	string accurate;

	/***** Signal Stats *****/

	float low_time;
	float low_altitude;
	float low_airspeed;
	float low_logID;

	float high_time;
	float high_altitude;
	float high_airspeed;
	float high_logID;

	/***** Flight Quality *****/

	float average_deviation_localizer;
	float average_deviation_glideslope;
	float average_deviation_both;

	float standard_deviation_localizer;
	float standard_deviation_glideslope;
	float standard_deviation_both;

	/***** Response Times *****/

	// Yoke Pitch Input
	float RT_pitch_raw;

	// Yoke Roll Input
	float RT_roll_raw;

	// Glideslope
	float RT_glideslope_raw;
	float RT_glideslope_vel;

	// Localizer
	float RT_localizer_raw;
	float RT_localizer_vel;

	float RT_horizontal;
	float RT_vertical;
	float RT_master;

	/***** Go-Around Quality *****/

	float standard_deviation_vertical_speed;
	int stall;
};