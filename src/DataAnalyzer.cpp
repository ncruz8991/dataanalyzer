#include "DataAnalyzer.h"

DataAnalyzer::DataAnalyzer(string filename_in, int cols_in, float devmult = 1.0)
	: filename(filename_in), cols(cols_in),
	  duration_after(2), m_mult(devmult)
{
	// Initialize the member variables
	stddev_vs = -1;
	did_stall = false;
	avgdev_localizer = -1;
	avgdev_glideslope = -1;
	avgdev_both = -1;
	stddev_localizer = -1;
	stddev_glideslope = -1;
	stddev_both = -1;
	m_avgStdDevPitch = -1;
	m_avgStdDevRoll = -1;

	string autopilot;
	LoadConfigFloat(thresholdDuration, "General", "thresholdDuration", DATA_CONFIG_PATH);
	LoadConfigString(autopilot, this->scenario_name, "autopilot", CONFIG_PATH);

	isAuto = autopilot == "on" ? true : false;

	if(!LoadConfigFloat(m_mult, "General", "deviationMultiplier", DATA_CONFIG_PATH))
		m_mult=1.0;
	for (int i = 0; i < TACTOR_COUNT; ++i)
	{
		signal_low.push_back(-1.0f);
		signal_high.push_back(-1.0f);
	}
	accurate = "n/a";

	for (int i = 0; i < RT_COUNT; ++i)
	{
		RT_pitch.push_back(-1.0f);
		RT_roll.push_back(-1.0f);
		RT_glideslope.push_back(-1.0f);
		RT_localizer.push_back(-1.0f);
	}
	
	// Open CSV and read files into vector
	csv::ifstream is((FILE_PREFIX + filename).c_str(), std::ios_base::in);
	is.set_delimiter(',');
	if(is.is_open())
	{
		cout << "Opening " << filename << "..." << endl;
		while(is.read_line())
		{
			CSVRow thisRow(cols);
			for (int i = 0; i < cols; ++i)
			{
				is >> thisRow[i];				
			}
			data.push_back(thisRow);
		}
		cout << "...Finished reading " << filename << endl << endl;
	}
	rows = data.size();

	// Store scenario name (for config)	
	scenario_name = filename;
	scenario_name.erase(14, 4); // Delete ".csv"
	scenario_name.erase(0, 4); // Delete "log_"

	// Initialize calculations vector	
	calculations.resize(CSV_CALCULATIONS_COUNT, vector<float>(rows, 0.0f));

	// Check if this scenario had a blunder
	LoadConfigString(blunder_dir, scenario_name, "blunder_dir", CONFIG_PATH);
	if (blunder_dir == "left" || blunder_dir == "right")
		hasBlunder = true;
	else
		hasBlunder = false;

	// Get participant name
	LoadConfigString(participant_name, "General", "participant", CONFIG_PATH);
}

void DataAnalyzer::run()
{
	if (rows > 0)
	{
		// Calculate some statistics during the relevant parts of the flight
		cout << endl << "Calculating flight quality statistics for " << scenario_name << "..." << endl;
		calculateRelevantFlightStatistics();
		cout << "Done!" << endl;

		// If a tactor signal went off, calculate the Go Around Quality
		if (hasBlunder)
		{
			cout << endl << "Calculating accuracy for " << scenario_name << "..." << endl;
			getAccuracy();
			cout << "Done!" << endl;

			cout << endl << "Calculating go-around quality for " << scenario_name << "..." << endl;
			calculateGoAroundQuality();
			cout << "Done!" << endl;

			cout << endl << "Calculating response times for " << scenario_name << "..." << endl;
			calculateResponseTimes();
			cout << "Done!" << endl;

			cout << endl << "Calculating tactor statistics for " << scenario_name << "..." << endl;
			getTactorStatistics();
			cout << "Done!" << endl;
		}
	}
}

void DataAnalyzer::runAvg(){
	if (rows > 0)
	{
		// Calculate some statistics during the relevant parts of the flight
		cout << endl << "Calculating flight quality statistics for " << scenario_name << "..." << endl;
		calculateRelevantFlightStatistics();
		cout << "Done!" << endl;

		// If a tactor signal went off, calculate the Go Around Quality
		if (hasBlunder)
		{
			cout << endl << "Calculating response times for " << scenario_name << "..." << endl;
			calculateResponseTimesAvg();
			cout << "Done!" << endl;
		}
	}
} 
//Calculates response time for autopilot runs: should be the difference in timestamps
//between the yoke input response and the time when the tactor signal
void DataAnalyzer::runAutoYokeResponse(){
	if (rows > 0)
	{
		// Calculate some statistics during the relevant parts of the flight
		cout << endl << "Calculating flight quality statistics for " << scenario_name << "..." << endl;
		calculateRelevantFlightStatistics();
		cout << "Done!" << endl;

		// If a tactor signal went off, calculate the Go Around Quality
		if (hasBlunder && isAuto)
		{
			cout << endl << "Calculating response times for " << scenario_name << "..." << endl;
			calculateYokeResponseTime();
			cout << "Done!" << endl;
		}
	}
}

ResultsPackage DataAnalyzer::finalizeResults()
{
	ResultsPackage tResults;
	LoadConfigString(tResults.alarm_type, "General", "alarm_type", CONFIG_PATH);
	tResults.participant = this->participant_name;
	tResults.scenario = this->scenario_name;
	tResults.scenario.erase(0, 9); // Delete "Scenario_"
	LoadConfigString(tResults.autopilot, this->scenario_name, "autopilot", CONFIG_PATH);
	LoadConfigString(tResults.surprise, this->scenario_name, "surprise", CONFIG_PATH);
	LoadConfigString(tResults.aural, this->scenario_name, "aural_cue", CONFIG_PATH);
	LoadConfigString(tResults.graded, this->scenario_name, "tactor_graded", CONFIG_PATH);
	LoadConfigString(tResults.runway, this->scenario_name, "runway", CONFIG_PATH);
	LoadConfigString(tResults.blunder, this->scenario_name, "blunder_dir", CONFIG_PATH);
	tResults.accurate = this->accurate;

	tResults.low_time = this->signal_low[TACTOR_TIME];
	tResults.low_altitude = this->signal_low[TACTOR_ALTITUDE];
	tResults.low_airspeed = this->signal_low[TACTOR_AIRSPEED];
	tResults.low_logID = this->signal_low[TACTOR_LOGID];

	tResults.high_time = this->signal_high[TACTOR_TIME];
	tResults.high_altitude = this->signal_high[TACTOR_ALTITUDE];
	tResults.high_airspeed = this->signal_high[TACTOR_AIRSPEED];
	tResults.high_logID = this->signal_high[TACTOR_LOGID];

	tResults.average_deviation_localizer = this->avgdev_localizer;
	tResults.average_deviation_glideslope = this->avgdev_glideslope;
	tResults.average_deviation_both = this->avgdev_both;

	tResults.standard_deviation_localizer = this->stddev_localizer;
	tResults.standard_deviation_glideslope = this->stddev_glideslope;
	tResults.standard_deviation_both = this->stddev_both;

	tResults.standard_deviation_vertical_speed = this->stddev_vs;
	tResults.stall = this->did_stall;

	tResults.RT_pitch_raw = this->RT_pitch[RT_RAW];
	tResults.RT_roll_raw = this->RT_roll[RT_RAW];
	tResults.RT_glideslope_raw = this->RT_glideslope[RT_RAW];
	tResults.RT_glideslope_vel = this->RT_glideslope[RT_VELOCITY];
	tResults.RT_localizer_raw = this->RT_localizer[RT_RAW];
	tResults.RT_localizer_vel = this->RT_localizer[RT_VELOCITY];

	if (hasBlunder)
	{
		tResults.RT_horizontal = this->RT_horizontal;
		tResults.RT_vertical = this->RT_vertical;
		tResults.RT_master = this->RT_final;
	}
	else
	{
		tResults.RT_horizontal = -1;
		tResults.RT_vertical = -1;
		tResults.RT_master = -1;
	}

	return tResults;
}


//Returns true if autopilot was not enabled. If the return value is false, we can
//expect the pre-tactor std deviation to be very low
bool DataAnalyzer::isManualRun(){
	ResultsPackage tResults;
	LoadConfigString(tResults.autopilot, this->scenario_name, "autopilot", CONFIG_PATH);
	return tResults.autopilot == "off";
}

void DataAnalyzer::getAccuracy()
{
	int last_index = data.size() - 1;

	bool isRight = true;
	if (blunder_dir == "left")
		isRight = false;

	bool wentRight = true;
	if (data[last_index][LOCALIZER] > 0)
		wentRight = false;

	if (isRight != wentRight)
		accurate = "correct";
	else
		accurate = "incorrect";

	// If they landed, then they missed it
	if (data[last_index][ALTITUDE] < 100)
		accurate = "missed";
}

void DataAnalyzer::calculateGoAroundQuality()
{

	// Calculate the vertical speed of every point
	int steps = 1;
	for (int row_i = (1 + steps); row_i < rows; ++row_i)
	{
		calculations[VERTICAL_SPEED][row_i] = getVerticalSpeed(row_i, steps);
	}

	// Find the set of indices that you need to work with (the index range)
	int dFirst = findTactorSignal();
	int dLast = findDurationEnd(dFirst);

	// Store the standard dev of the vertical speeds in that duration in stddev_vs
	stddev_vs = getSTDDEV(dFirst, dLast, VERTICAL_SPEED);
	
	// Check if the pilot stalled (dipped below 100 kias) in that duration
	for (int i = dFirst; i <= dLast; ++i)
	{
		if (data[i][SPEED] <= 100)
			did_stall = true;
	}
}

int DataAnalyzer::findTactorSignal(bool high)
{
	int column;
	if (high)
		column = SENT_HIGH_SIGNAL;
	else
		column = SENT_LOW_SIGNAL;

	int tactor_index = 0;

	bool found_signal = false;
	for (int i = 1; !found_signal && i < rows; ++i)
	{
		if (data[i][column] == 1)
		{
			tactor_index = i;
			found_signal = true;
		}
	}

	return tactor_index;
}

int DataAnalyzer::findDurationEnd(int indexSignal)
{
	int end_index = 0;
	float tactor_time = data[indexSignal][TIME];
	bool found_end = false;
	for (int i = indexSignal + 1; !found_end && indexSignal < rows; ++i)
	{
		if (data[i][TIME] - tactor_time > 2)
		{
			end_index = i;
			found_end = true;
		}
	}
	return end_index;
}

float DataAnalyzer::getVerticalSpeed(int i, int steps)
{
	float a2 = data[i][ALTITUDE];
	float t2 = data[i][TIME];
	float a1 = data[i - steps][ALTITUDE];
	float t1 = data[i - steps][TIME];

	return 60 * (a2 - a1) / (t2 - t1);
}

void DataAnalyzer::calculateRelevantFlightStatistics()
{
	int start = findRelevantFlightStart(0.1f);
	int end = findRelevantFlightEnd(start);

	// Calculate deviations of Localizer and Glideslope
	for (int i = start; i <= end; ++i)
	{
		calculations[LOC_DEV][i] = fabsf(data[i][LOCALIZER]);
		calculations[GS_DEV][i] = fabsf(data[i][GLIDESLOPE]);

		calculations[LOC_GS_DEV][i]
			= calculations[LOC_DEV][i] + calculations[GS_DEV][i];
		calculations[LOC_GS_DEV][i] /= 2;
	}

	// Get statistics from them
	avgdev_localizer = getAverage(start, end, LOC_DEV);
	avgdev_glideslope = getAverage(start, end, GS_DEV);
	avgdev_both = getAverage(start, end, LOC_GS_DEV);
	stddev_localizer = getSTDDEV(start, end, LOC_DEV);
	stddev_glideslope = getSTDDEV(start, end, GS_DEV);
	stddev_both = getSTDDEV(start, end, LOC_GS_DEV);
}

int DataAnalyzer::findRelevantFlightStart(float GSmin)
{
	float GSminpos = fabsf(GSmin);
	float GSminneg = GSminpos * -1;

	int i = 1;
	bool foundStart = false;
	while (!foundStart && i < (rows - 1))
	{
		float gs = data[i][GLIDESLOPE];
		if (gs > GSminneg && gs < GSminpos)
		{
			foundStart = true;
			break;
		}
		++i;
	}
	return i;
}

int DataAnalyzer::findRelevantFlightEnd(int start_index)
{
	int i = start_index;
	bool foundEnd = false;
	while (!foundEnd && i < (rows - 1))
	{
		if (hasBlunder && data[i][SENT_HIGH_SIGNAL] == 1)
		{
			foundEnd = true;
			break;
		}
		else if (data[i][ALTITUDE] < 600)
		{
			foundEnd = true;
			break;
		}
		++i;
	}
	return i;
}

void DataAnalyzer::calculateResponseTimes()
{
	// Find the index for X seconds before the tactor signal
	// Note: We get the end first because we are counting backwards
	// from the tactor signal
	tEnd = findTactorSignal();
	tStart = findThresholdStart();

	// All deviations should be from 0 EXCEPT vertical speed and pitch and roll
	// So find the average vertical speed between tEnd and tStart
	float avgYP = getAverage(tStart, tEnd, PITCH);
	float avgYR = getAverage(tStart, tEnd, ROLL);

	// Calculate all column deviations (turn them to absolutes later)
	for (int i = 1; i < rows; ++i)
	{
		calculations[RAW_PITCH][i] = data[i][PITCH];
		calculations[RAW_ROLL][i] = data[i][ROLL];
		calculations[RAW_GLIDESLOPE][i] = data[i][GLIDESLOPE];
		calculations[RAW_LOCALIZER][i] = data[i][LOCALIZER];

		calculations[PITCH_DEV][i] = data[i][PITCH] - avgYP;
		calculations[ROLL_DEV][i] = data[i][ROLL] - avgYR;
		calculations[GS_DEV][i] = data[i][GLIDESLOPE];
		calculations[LOC_DEV][i] = data[i][LOCALIZER];
	}
	
	// Calculate all column velocities
	calculateColumnDerivative(V_GLIDESLOPE, GLIDESLOPE);
	calculateColumnDerivative(V_LOCALIZER, LOCALIZER);

	// Save the raw calculations
	for (int i = 1; i < rows; ++i)
	{
		calculations[RAW_V_GLIDESLOPE][i] = calculations[V_GLIDESLOPE][i];
		calculations[RAW_V_LOCALIZER][i] = calculations[V_LOCALIZER][i];
	}

	// Turn all of those values into deviations (absolute values)
	// We do this last so we don't mess the derivative calculations
	for (int i = 1; i < rows; ++i)
		for (int column = PITCH_DEV; column <= V_LOCALIZER; ++column)
			calculations[column][i] = fabsf(calculations[column][i]);

	// Find the average deviation of each column between tStart and tEnd
	// That is the threshold that we will stay under until the response
	for (int i = 0; i < CSV_CALCULATIONS_COUNT; ++i)
		thresholds.push_back(0);

	for (int i = PITCH_DEV; i <= V_LOCALIZER; ++i)
		calculateThreshold((CSV_CALCULATIONS) i);

	// Find the response time based on that threshold
	// 1) Pitch RT (from raw data)
	RT_pitch[RT_RAW] = findResponseTime(PITCH_DEV);

	// 2) Roll RT (from raw data)
	RT_roll[RT_RAW] = findResponseTime(ROLL_DEV);

	// 3) GS deviation RT (from raw data)
	// 4) GS deviation RT (from speed)
	RT_glideslope[RT_RAW] = findResponseTime(GS_DEV);
	RT_glideslope[RT_VELOCITY] = findResponseTime(V_GLIDESLOPE);

	// 5) Localizer deviation RT (from raw data) 
	// 6) Localizer deviation RT (from speed)
	RT_localizer[RT_RAW] = findResponseTime(LOC_DEV);
	RT_localizer[RT_VELOCITY] = findResponseTime(V_LOCALIZER);

	// 7) Average of 1) and 3) and 4) (vertical RT)
	vector<float> RT_vertical_averages;
	RT_vertical_averages.push_back(RT_pitch[RT_RAW]);
	RT_vertical_averages.push_back(RT_glideslope[RT_RAW]);
	RT_vertical_averages.push_back(RT_glideslope[RT_VELOCITY]);
	RT_vertical_averages.push_back(0.0f);
	getAverageRT(RT_vertical_averages, 3);
	RT_vertical = RT_vertical_averages[3];


	// 8) Average of 2) and 5) and 6) (Horizontal RT)
	vector<float> RT_horizontal_averages;
	RT_horizontal_averages.push_back(RT_pitch[RT_RAW]);
	RT_horizontal_averages.push_back(RT_localizer[RT_RAW]);
	RT_horizontal_averages.push_back(RT_localizer[RT_VELOCITY]);
	RT_horizontal_averages.push_back(0.0f);
	getAverageRT(RT_horizontal_averages, 3);
	RT_horizontal = RT_horizontal_averages[3];

	// 9) Average of 7) and 8) (Overall RT)
	vector<float> RT_final_averages;
	RT_final_averages.push_back(RT_horizontal);
	RT_final_averages.push_back(RT_vertical);
	RT_final_averages.push_back(0.0f);
	getAverageRT(RT_final_averages, 2);
	RT_final = RT_final_averages[2];
}

void DataAnalyzer::calculateResponseTimesAvg(){
	// Find the index for X seconds before the tactor signal
	// Note: We get the end first because we are counting backwards
	// from the tactor signal
	tEnd = findTactorSignal();
	tStart = findThresholdStart();

	// All deviations should be from 0 EXCEPT vertical speed and pitch and roll
	// So find the average vertical speed between tEnd and tStart
	float avgYP = getAverage(tStart, tEnd, PITCH);
	float avgYR = getAverage(tStart, tEnd, ROLL);

	// Calculate all column deviations (turn them to absolutes later)
	for (int i = 1; i < rows; ++i)
	{
		
		calculations[RAW_PITCH][i] = data[i][PITCH];
		calculations[RAW_ROLL][i] = data[i][ROLL];
		calculations[RAW_GLIDESLOPE][i] = data[i][GLIDESLOPE];
		calculations[RAW_LOCALIZER][i] = data[i][LOCALIZER];

		calculations[PITCH_DEV][i] = data[i][PITCH] -avgYP;
		calculations[ROLL_DEV][i] = data[i][ROLL] - avgYR;
		calculations[GS_DEV][i] = data[i][GLIDESLOPE];
		calculations[LOC_DEV][i] = data[i][LOCALIZER];
	}
	
	// Calculate all column velocities
	calculateColumnDerivative(V_GLIDESLOPE, GLIDESLOPE);
	calculateColumnDerivative(V_LOCALIZER, LOCALIZER);

	// Save the raw calculations
	for (int i = 1; i < rows; ++i)
	{
		calculations[RAW_V_GLIDESLOPE][i] = calculations[V_GLIDESLOPE][i];
		calculations[RAW_V_LOCALIZER][i] = calculations[V_LOCALIZER][i];
	}

	// Turn all of those values into deviations (absolute values)
	// We do this last so we don't mess the derivative calculations
	for (int i = 1; i < rows; ++i)
		for (int column = PITCH_DEV; column <= V_LOCALIZER; ++column)
			calculations[column][i] = fabsf(calculations[column][i]);

	// Find the average deviation of each column between tStart and tEnd
	// That is the threshold that we will stay under until the response
	for (int i = 0; i < CSV_CALCULATIONS_COUNT; ++i)
		thresholds.push_back(0);

	//This sets the threshold value equal to what was originally passed in
	//This should use the average std deviation found over multiple runs
	//to calculate the response time
	thresholds[PITCH_DEV] = m_avgStdDevPitch; 
	thresholds[ROLL_DEV] = m_avgStdDevRoll;
	for (int i = GS_DEV; i <= V_LOCALIZER; ++i)
		calculateThreshold((CSV_CALCULATIONS) i);

	// Find the response time based on that threshold
	// 1) Pitch RT (from raw data)
	RT_pitch[RT_RAW] = findResponseTime(PITCH_DEV);

	// 2) Roll RT (from raw data)
	RT_roll[RT_RAW] = findResponseTime(ROLL_DEV);

	// 3) GS deviation RT (from raw data)
	// 4) GS deviation RT (from speed)
	RT_glideslope[RT_RAW] = findResponseTime(GS_DEV);
	RT_glideslope[RT_VELOCITY] = findResponseTime(V_GLIDESLOPE);

	// 5) Localizer deviation RT (from raw data) 
	// 6) Localizer deviation RT (from speed)
	RT_localizer[RT_RAW] = findResponseTime(LOC_DEV);
	RT_localizer[RT_VELOCITY] = findResponseTime(V_LOCALIZER);

	// 7) Average of 1) and 3) and 4) (vertical RT)
	vector<float> RT_vertical_averages;
	RT_vertical_averages.push_back(RT_pitch[RT_RAW]);
	RT_vertical_averages.push_back(RT_glideslope[RT_RAW]);
	RT_vertical_averages.push_back(RT_glideslope[RT_VELOCITY]);
	RT_vertical_averages.push_back(0.0f);
	getAverageRT(RT_vertical_averages, 3);
	RT_vertical = RT_vertical_averages[3];


	// 8) Average of 2) and 5) and 6) (Horizontal RT)
	vector<float> RT_horizontal_averages;
	RT_horizontal_averages.push_back(RT_pitch[RT_RAW]);
	RT_horizontal_averages.push_back(RT_localizer[RT_RAW]);
	RT_horizontal_averages.push_back(RT_localizer[RT_VELOCITY]);
	RT_horizontal_averages.push_back(0.0f);
	getAverageRT(RT_horizontal_averages, 3);
	RT_horizontal = RT_horizontal_averages[3];

	// 9) Average of 7) and 8) (Overall RT)
	vector<float> RT_final_averages;
	RT_final_averages.push_back(RT_horizontal);
	RT_final_averages.push_back(RT_vertical);
	RT_final_averages.push_back(0.0f);
	getAverageRT(RT_final_averages, 2);
	RT_final = RT_final_averages[2];
}

void DataAnalyzer::calculateYokeResponseTime(){
	// Find the index for X seconds before the tactor signal
	// Note: We get the end first because we are counting backwards
	// from the tactor signal
	tEnd = findTactorSignal();
	tStart = findThresholdStart();
}

void DataAnalyzer::calculateColumnDerivative(CSV_CALCULATIONS result, CSV_COLUMN column)
{
	int steps = 1;
	for (int row_i = (1 + steps); row_i < rows; ++row_i)
	{
		calculations[result][row_i] = getDerivative(column, row_i, steps);
	}
}

void DataAnalyzer::calculateColumnDerivative(CSV_CALCULATIONS result, CSV_CALCULATIONS column)
{
	int steps = 1;
	for (int row_i = (1 + steps); row_i < rows; ++row_i)
	{
		calculations[result][row_i] = getDerivative(column, row_i, steps);
	}
}

int DataAnalyzer::findThresholdStart()
{
	// From the tactor_signal index, count backwards
	float tactor_time = data[tEnd][TIME];
	int i = tEnd;
	while (i > 0)
	{
		if (tactor_time - data[i][TIME] > thresholdDuration)
			break;
		--i;
	}
	return i;
}

void DataAnalyzer::calculateThreshold(CSV_CALCULATIONS column)
{
	float sum = 0;
	for (int i = tStart; i <= tEnd; ++i)
	{
		sum += calculations[column][i];
	}
	// This is the average deviation in that time period
	thresholds[column] = sum / (tEnd - tStart + 1);
}

float DataAnalyzer::findResponseTime(CSV_CALCULATIONS column)
{
	int rawColumn = column - 6;
	float baseValue = calculations[rawColumn][tEnd];
	float upperLimit = baseValue + (m_mult * thresholds[column]);
	float lowerLimit = baseValue - (m_mult * thresholds[column]);

	// Start from the tactor signal.
	// When the variable's value becomes higher than its current value
	// (value when the tactile cue was sent) +/-the average deviation,
	// stop and record the response time,
	int i = tEnd + 1;
	while (i < (rows - 1))
	{
		float thisValue = calculations[rawColumn][i];
		if (thisValue > upperLimit || thisValue < lowerLimit)
		//if (calculations[column][i] > thresholds[column])
			break;
		else
			++i;
	}
	return data[i][TIME] - data[tEnd][TIME];
}

void DataAnalyzer::getAverageRT(vector<float>& RT_vector, int avg_index)
{
	float minimumResponseTime = 0.3f;
	int valuesAdded = 0;
	RT_vector[avg_index] = 0;
	for (int i = 0; i < avg_index; ++i)
	{
		if (RT_vector[i] > minimumResponseTime)
		{
			RT_vector[avg_index] += RT_vector[i];
			++valuesAdded;
		}
	}
	if (valuesAdded == 0)
		RT_vector[avg_index] = -1.0f;
	else
		RT_vector[avg_index] /= valuesAdded;
}

float DataAnalyzer::getDerivative(CSV_COLUMN column, int i, int steps)
{
	float y2 = data[i][column];
	float t2 = data[i][TIME];
	float y1 = data[i - steps][column];
	float t1 = data[i - steps][TIME];

	return (y2 - y1) / (t2 - t1);
}

float DataAnalyzer::getDerivative(CSV_CALCULATIONS column, int i, int steps)
{
	float y2 = calculations[column][i];
	float t2 = data[i][TIME];
	float y1 = calculations[column][i - steps];
	float t1 = data[i - steps][TIME];

	return (y2 - y1) / (t2 - t1);
}

void DataAnalyzer::getTactorStatistics()
{	
	if (hasBlunder)
	{
		int high = findTactorSignal();
		signal_high[TACTOR_TIME] = data[high][TIME];
		signal_high[TACTOR_ALTITUDE] = data[high][ALTITUDE];
		signal_high[TACTOR_AIRSPEED] = data[high][SPEED];
		signal_high[TACTOR_LOGID] = data[high][LOG_ID];

		string graded;
		LoadConfigString(graded, scenario_name, "tactor_graded", CONFIG_PATH);
		if (graded == "graded")
		{
			int low = findTactorSignal(false);
			signal_low[TACTOR_TIME] = data[low][TIME];
			signal_low[TACTOR_ALTITUDE] = data[low][ALTITUDE];
			signal_low[TACTOR_AIRSPEED] = data[low][SPEED];
			signal_low[TACTOR_LOGID] = data[low][LOG_ID];
		}
	}
}

float DataAnalyzer::getAverage(int first, int last, CSV_CALCULATIONS column)
{
	float sum = 0;
	for (int i = first; i <= last; ++i)
	{
		sum += calculations[column][i];
	}
	return sum / (last - first + 1);
}

float DataAnalyzer::getAverage(int first, int last, CSV_COLUMN column)
{
	float sum = 0;
	for (int i = first; i <= last; ++i)
	{
		sum += data[i][column];
	}
	return sum / (last - first + 1);
}

float DataAnalyzer::getSTDDEV(int first, int last, CSV_CALCULATIONS column)
{
	float mean = getAverage(first, last, column);
	float sum_deviation = 0;
	int n = last - first + 1;
	for (int i = first; i <= last; ++i)
	{
		float deviation = calculations[column][i] - mean;
		sum_deviation += (deviation * deviation);
	}
	return sqrt(sum_deviation / n);
}