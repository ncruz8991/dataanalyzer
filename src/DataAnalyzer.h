#pragma once
#include <string>
#include <vector>
#include <unordered_map>
#include <thread>
#include <iostream>

#include "minicsv15\MiniCSV\minicsv.h"
#include "Config.h"
#include "enums.h"
#include "Data.h"

using namespace std;

static const string FILE_PREFIX = "./Logs/";

class CSVRow
{
public:
	CSVRow() {}

	CSVRow(int cols_in)
	{
		cells.resize(cols_in);
	}

	float& operator[](int index) { return cells[index]; };

private:
	vector<float> cells;
};

// There is ONE dataanalyzer per .csv. They are paired.
class DataAnalyzer
{
public:
	DataAnalyzer();

	DataAnalyzer(string filename_in, int numCols_in, float devmult);

	int getRowsCount() { return rows; }
	int getColsCount() { return cols; }

	void run();
	void runAvg();
	void runAutoYokeInput();

	// Write all the final data to the header file
	ResultsPackage finalizeResults();

	CSVRow& operator[](int index) { return data[index]; };

	bool isManualRun();
	bool HasBlunder(){return hasBlunder;}

	void setAvgStdDevPitch(float avgdevpitch){m_avgStdDevPitch = avgdevpitch;}
	void setAvgStdDevRoll(float avgdevroll){m_avgStdDevRoll = avgdevroll;}

	float getPitchDev(){ return thresholds[PITCH_DEV];}
	float getRollDev(){return thresholds[ROLL_DEV];}

private:
	string participant_name;
	string filename;
	string scenario_name;
	string blunder_dir;
	bool hasBlunder;
	bool isAuto;
	string accurate;
	int cols;
	int rows;
	float m_mult;
	vector<CSVRow> data;

	// Sets accurate to true or false depending on the blunder and response
	void getAccuracy();

	/******
	 * GO-AROUND QUALITY
	 * 
	 * START: Tactor signal
	 *   END: duration_after seconds after signal
	 *
	 * Things to calculate for duration:
	 * 1. Standard Deviation of Vertical Speed
	 * 2. Stall Flag - (Did they reach below 100 kias)
	 */

	float duration_after;
	float stddev_vs;
	bool did_stall;

	// Calculates the go-around quality of the flight
	// This is only called if there was a tactor signal
	void calculateGoAroundQuality();

	// Returns the index of the row where the tactor signal turns on
	int findTactorSignal(bool high = true);

	// Returns the index of the last row in the relevant duration
	int findDurationEnd(int indexSignal);

	float getVerticalSpeed(int i, int steps);

	/******
	 * RELEVANT FLIGHT STATISTICS
	 * 
	 * START: Glideslope intercept
	 *   END: Tactor signal, or altitude 600 ft (whichever comes first)
	 *
	 * Things to calculate for duration:
	 * 1. Average Deviation
	 *		a. of Localizer
	 *		b. of Glideslope
	 *		c. of both
	 * 2. Standard Deviation
	 *		a. of Localizer
	 *		b. of Glideslope
	 *		c. of both
	 */
	float avgdev_localizer;
	float avgdev_glideslope;
	float avgdev_both;
	float stddev_localizer;
	float stddev_glideslope;
	float stddev_both;

	// Every flight should calculate these statistics
	void calculateRelevantFlightStatistics();

	// Returns the index of the start of the duration
	// GSmin is the mininum deviation in the Glideslope that determin
	// when the "glideslope intercept" occurs.
	int findRelevantFlightStart(float GSmin);

	// Returns the index of the end of the duration
	// The duration ends at the first tactor signal, or 600 feet,
	// whichever comes first.
	int findRelevantFlightEnd(int start_index);

	/******
	 * Response Time
	 * 
	 * START: Tactor signal (hasBlunder == true)
	 *   END: Tactor signal, or altitude 600 ft (whichever comes first)
	 *
	 * Too complicated see emails...
	 */

	// Use enum RT
	// There will be 4 RTs per vector:
	// RT_RAW: from the average deviation around average
	// RT_VELOCITY: from the average deviation around change in raw
	// RT_AVERAGE: average of the previous 3
	vector<float> RT_pitch;
	vector<float> RT_roll;
	vector<float> RT_glideslope;
	vector<float> RT_localizer;
	float RT_vertical;
	float RT_horizontal;
	float RT_final;

	float m_avgStdDevPitch;
	float m_avgStdDevRoll;

	// thresholdDuration is a preset number (to be decided)
	// tStart is thresholdDuration seconds before tEnd
	// tEnd is the tactor signal
	float thresholdDuration;
	int tStart;
	int tEnd;
	vector<float> thresholds;

	// Only call this if there's a blunder
	void calculateResponseTimes();
	void calculateResponseTimesAvg();

	void calculateColumnDerivative(CSV_CALCULATIONS result, CSV_COLUMN column);
	void calculateColumnDerivative(CSV_CALCULATIONS result, CSV_CALCULATIONS column);

	float getDerivative(CSV_COLUMN column, int i, int steps);
	float getDerivative(CSV_CALCULATIONS column, int i, int steps);

	int findThresholdStart();

	// Finds average deviation of values between tStart and tEnd (inclusive)
	void calculateThreshold(CSV_CALCULATIONS column);

	float findResponseTime(CSV_CALCULATIONS column);
	float findResponseTimeAvg(CSV_CALCULATIONS column);

	// avg_index is the index where the average number should go
	void getAverageRT(vector<float>& RT_vector, int avg_index = RT_AVERAGE);

	/******
	 * Tactor Signal info
	 *
	 * For each signal, (graded and non-graded)
	 * Time
	 * Altitude
	 * Airspeed
	 * LogID
	 */

	 // Use these with enum TACTOR_SIGNAL
	 vector<float> signal_low;
	 vector<float> signal_high;

	 void getTactorStatistics();

	/******************/

	// Use with CSV_CALCULATIONS enum
	// calculations[CSV_CALCULATIONS][row]
	vector<vector<float>> calculations;

	// Returns the average of a column in calculations
	float getAverage(int first, int last, CSV_CALCULATIONS column);
	float getAverage(int first, int last, CSV_COLUMN column);

	// Returns standard deviation of a column in calculations
	float getSTDDEV(int first, int last, CSV_CALCULATIONS column);
};